package com.cannyquest.clearing;

import org.jpos.iso.ISOMsg;
import org.jpos.iso.packager.GenericPackager;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 * the purpose of this class is to handle the data read from the raw file, remove the unused bytes and convert it into
 * ISOMsg objects for further processing.
 */


public class Reader {

    private ByteBuffer buffer;


    /**
     * list that holds the ISO Messages objects
     */

    int msgCount = 0;
    private int len = 0;
    private int blockIndex = 1;
    private int blockSize = 1014;
    private int msgIndex = 1;
    private ISOMsg tmp = new ISOMsg();
    private List<ISOMsg> msgList = new ArrayList<>();
    private GenericPackager genericPackager;



    /**
     * creates an instance of the reader and removes the unused bytes.
     * @param src raw data from the file
     */
    public Reader (ByteBuffer src, String pathtoPackager) throws Exception {


        genericPackager = new GenericPackager(pathtoPackager);

        len = src.getInt();
        byte[] bytes = null;
        if (len == 0) {
            throw new Exception("Invalid Header Length...");
        }

        /**
         * reading the header from the buffer
         */
        if (len < blockSize) {
            bytes = new byte[len];
            src.get(bytes, 0, len);

            tmp = new ISOMsg();
            genericPackager.unpack(tmp,bytes);
            msgList.add(tmp);

        } else {
            int mark = (1012 * blockIndex) - src.position();
            src.get(bytes, 0, mark);
            src.position(src.position() + 2);
            src.get(bytes, mark, len - mark);
            blockIndex++;
        }

        /**
         * loop on the remaining data in the buffer tp read the rest of the messages one by one
         */
        while (true) {

            /**
             * get the length of the message
             *
             */
            //@TODO must check the block size in case we are 4 bytes before the dirty bytes.
            len = src.getInt();

            /**
             * if the length == ZERO hence no more messages to be read, break the loop.
             */
            if (len == 0) {
                break;
            }

            /**
             * define byte array with the same size as the message length
             */
            bytes = new byte[len];
            /**
             * handle the unused bytes at the end of 1014 block.
             */
            if ((src.position() + len) < (blockIndex * 1014)) {
                src.get(bytes, 0, len);
            } else if ((src.position()+len)>= ((blockIndex*blockSize)+1)){
                //System.out.println("block exceeded... you are in block: "+blockIndex);
                int mark = (blockSize * blockIndex)-2 - src.position();
                int remainder = len-mark;
                src.get(bytes, 0, mark);
                src.position(src.position() + 2);
                src.get(bytes, mark, remainder);
                blockIndex++;
            }

            /**
             * create a new ISOMsg in the list, assign the packager then unpack the byte array into the message.
             */
            tmp = new ISOMsg();
            genericPackager.unpack(tmp,bytes);
            msgList.add(tmp);
            msgIndex++;
        }
    }

    /**
     * getter to retrieve the list of ISO Messages for further processing
     * @return list of ISOMsg objects
     */

    public List<ISOMsg> getIsoMsgs() {
        return msgList;
    }

}
