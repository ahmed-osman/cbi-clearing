package com.cannyquest.clearing;
import org.jpos.iso.*;

public class IPMPackager extends ISOBasePackager {



    /**

     * SVBO  Packager

     *

     * @author ahmed osman

     *

     * @see ISOPackager

     * @see ISOBasePackager

     * @see ISOComponent

     */



        private static final boolean pad = false;

        protected ISOFieldPackager fld[] = {

                new IFE_NUMERIC (  4, "MESSAGE TYPE INDICATOR"),

                new IFB_BITMAP  ( 16, "BIT MAP"),

                new IFE_LLNUM   ( 19, "PAN - PRIMARY ACCOUNT NUMBER"),

                new IFE_NUMERIC (  6, "PROCESSING CODE"),

                new IFE_NUMERIC ( 12, "AMOUNT, TRANSACTION"),

                new IFE_NUMERIC ( 12, "AMOUNT, SETTLEMENT"),

                new IFE_NUMERIC ( 12, "AMOUNT, CARDHOLDER BILLING"),

                new IFE_NUMERIC ( 10, "TRANSMISSION DATE AND TIME"),

                new IFE_NUMERIC (  8, "AMOUNT, CARDHOLDER BILLING FEE"),

                new IFE_NUMERIC (  8, "CONVERSION RATE, SETTLEMENT"),

                new IFE_NUMERIC (  8, "CONVERSION RATE, CARDHOLDER BILLING"),

                new IFE_NUMERIC (  6, "SYSTEM TRACE AUDIT NUMBER"),

                new IFE_NUMERIC (  12, "TIME, LOCAL TRANSACTION"),

                new IFE_NUMERIC (  4, "DATE, LOCAL TRANSACTION"),

                new IFE_NUMERIC (  4, "DATE, EXPIRATION"),

                new IFE_NUMERIC (  4, "DATE, SETTLEMENT"),

                new IFE_NUMERIC (  4, "DATE, CONVERSION"),

                new IFE_NUMERIC (  4, "DATE, CAPTURE"),

                new IFE_NUMERIC (  4, "MERCHANTS TYPE"),

                new IFE_NUMERIC (  3, "ACQUIRING INSTITUTION COUNTRY CODE"),

                new IFE_NUMERIC (  3, "PAN EXTENDED COUNTRY CODE"),

                new IFE_NUMERIC (  3, "FORWARDING INSTITUTION COUNTRY CODE"),

                new IFE_CHAR (  12, "POINT OF SERVICE ENTRY MODE"),

                new IFE_NUMERIC (  3, "CARD SEQUENCE NUMBER"),

                new IFE_NUMERIC (  3, "FUNCTION CODE"),

                new IFE_NUMERIC (  4, "REASON CODE"),

                new IFE_NUMERIC (  4, "Card Acceptor Business Code (MCC)"),

                new IFB_NUMERIC (  1, "27",true),

                new IFE_AMOUNT  (  9, "AMOUNT, TRANSACTION FEE"),

                new IFE_AMOUNT  (  9, "AMOUNT, SETTLEMENT FEE"),

                new IFE_AMOUNT  (  24, "AMOUNT ORIGINAL"),

                new IFE_LLNUM  (  23, "ACQUIRER REFERENCE DATA"),

                new IFE_LLNUM   ( 11, "ACQUIRING INSTITUTION IDENT CODE"),

                new IFE_LLNUM   ( 11, "FORWARDING INSTITUTION IDENT CODE"),

                new IFE_LLCHAR  ( 11, "PAN EXTENDED"),

                new IFB_LLNUM   ( 37, "TRACK 2 DATA", pad),

                new IFB_LLLCHAR (104, "TRACK 3 DATA"),

                new IFE_CHAR     ( 12, "RETRIEVAL REFERENCE NUMBER"),

                new IFE_CHAR     (  6, "AUTHORIZATION IDENTIFICATION RESPONSE"),

                new IF_CHAR     (  2, "RESPONSE CODE"),

                new IFE_CHAR     (  3, "SERVICE RESTRICTION CODE"),

                new IFE_CHAR     (  8, "CARD ACCEPTOR TERMINAL IDENTIFICACION"),

                new IFE_CHAR     ( 15, "CARD ACCEPTOR IDENTIFICATION CODE" ),

                new IFE_LLCHAR     ( 99, "CARD ACCEPTOR NAME/LOCATION"),

                new IFE_LLCHAR  ( 25, "ADITIONAL RESPONSE DATA"),

                new IFE_LLCHAR  ( 76, "TRACK 1 DATA"),

                new IFE_LLLCHAR (999, "ADITIONAL DATA - ISO"),

                new IFE_LLLCHAR (999, "ADITIONAL DATA - NATIONAL"),

                new IFE_LLLCHAR (999, "ADITIONAL DATA - PRIVATE"),

                new IFE_CHAR     (  3, "CURRENCY CODE, TRANSACTION"),

                new IFE_CHAR     (  3, "CURRENCY CODE, SETTLEMENT"),

                new IFE_CHAR     (  3, "CURRENCY CODE, CARDHOLDER BILLING"   ),

                new IFE_BINARY  (  8, "PIN DATA"   ),

                new IFE_NUMERIC ( 16, "SECURITY RELATED CONTROL INFORMATION"),

                new IFE_LLLCHAR (120, "ADDITIONAL AMOUNTS"),

                new IFE_LLLBINARY (999, "ICC DATA"),

                new IFE_LLLCHAR (999, "RESERVED ISO"),

                new IFE_LLLCHAR (999, "RESERVED NATIONAL"),

                new IFE_LLLCHAR (999, "RESERVED NATIONAL"),

                new IFE_LLLCHAR (999, "RESERVED NATIONAL"),

                new IFE_LLLCHAR (999, "RESERVED PRIVATE"),

                new IFE_LLLCHAR (999, "RESERVED PRIVATE"),

                new IFE_LLLCHAR (999, "RESERVED PRIVATE"),

                new IFE_LLLCHAR (999, "RESERVED PRIVATE"),

                new IFE_BINARY  (  8, "MESSAGE AUTHENTICATION CODE FIELD"),

                new IFE_BINARY  (  1, "BITMAP, EXTENDED"),

                new IFE_NUMERIC (  1, "SETTLEMENT CODE"),

                new IFE_NUMERIC (  2, "EXTENDED PAYMENT CODE"),

                new IFE_NUMERIC (  3, "RECEIVING INSTITUTION COUNTRY CODE"),

                new IFE_NUMERIC (  3, "SETTLEMENT INSTITUTION COUNTRY CODE"),

                new IFE_NUMERIC (  3, "NETWORK MANAGEMENT INFORMATION CODE"),

                new IFE_NUMERIC (  4, "MESSAGE NUMBER"),

                new IFE_NUMERIC (  4, "MESSAGE NUMBER LAST"),

                new IFE_NUMERIC (  6, "DATE ACTION"),

                new IFE_NUMERIC ( 10, "CREDITS NUMBER"),

                new IFE_NUMERIC ( 10, "CREDITS REVERSAL NUMBER"),

                new IFE_NUMERIC ( 10, "DEBITS NUMBER"),

                new IFE_NUMERIC ( 10, "DEBITS REVERSAL NUMBER"),

                new IFE_NUMERIC ( 10, "TRANSFER NUMBER"),

                new IFE_NUMERIC ( 10, "TRANSFER REVERSAL NUMBER"),

                new IFE_NUMERIC ( 10, "INQUIRIES NUMBER"),

                new IFE_NUMERIC ( 10, "AUTHORIZATION NUMBER"),

                new IFE_NUMERIC ( 12, "CREDITS, PROCESSING FEE AMOUNT"),

                new IFE_NUMERIC ( 12, "CREDITS, TRANSACTION FEE AMOUNT"),

                new IFE_NUMERIC ( 12, "DEBITS, PROCESSING FEE AMOUNT"),

                new IFE_NUMERIC ( 12, "DEBITS, TRANSACTION FEE AMOUNT"),

                new IFE_NUMERIC ( 16, "CREDITS, AMOUNT" ),

                new IFE_NUMERIC ( 16, "CREDITS, REVERSAL AMOUNT" ),

                new IFE_NUMERIC ( 16, "DEBITS, AMOUNT" ),

                new IFE_NUMERIC ( 16, "DEBITS, REVERSAL AMOUNT" ),

                new IFE_NUMERIC ( 42, "ORIGINAL DATA ELEMENTS" ),

                new IFE_CHAR     (  1, "FILE UPDATE CODE"),

                new IFE_CHAR     (  2, "FILE SECURITY CODE"),

                new IFE_CHAR     (  11, "RESPONSE INDICATOR"),

                new IFE_CHAR     (  11, "SERVICE INDICATOR"),

                new IFE_CHAR     ( 42, "REPLACEMENT AMOUNTS"),

                new IFE_BINARY  ( 16, "MESSAGE SECURITY CODE"),

                new IFE_AMOUNT  ( 17, "AMOUNT, NET SETTLEMENT"),

                new IFE_CHAR     ( 25, "PAYEE"),

                new IFE_LLNUM   ( 99, "SETTLEMENT INSTITUTION IDENT CODE"),

                new IFE_LLNUM   ( 11, "RECEIVING INSTITUTION IDENT CODE"),

                new IFE_LLCHAR  ( 99, "FILE NAME"),

                new IFE_LLCHAR  ( 28, "ACCOUNT IDENTIFICATION 1"),

                new IFE_LLCHAR  ( 28, "ACCOUNT IDENTIFICATION 2"),

                new IFE_LLLCHAR (100, "TRANSACTION DESCRIPTION"),

                new IFE_LLLCHAR (999, "RESERVED ISO USE"),

                new IFE_LLLCHAR (999, "RESERVED ISO USE"),

                new IFE_LLLCHAR (999, "RESERVED ISO USE"),

                new IFE_LLLCHAR (999, "RESERVED ISO USE"),

                new IFE_LLLCHAR (999, "RESERVED ISO USE"),

                new IFE_LLLCHAR (999, "RESERVED ISO USE"),

                new IFE_LLLCHAR (999, "RESERVED ISO USE"),

                new IFE_LLLCHAR (999, "RESERVED NATIONAL USE"),

                new IFE_LLLCHAR (999, "RESERVED NATIONAL USE"),

                new IFE_LLLCHAR (999, "RESERVED NATIONAL USE"   ),

                new IFE_LLLCHAR (999, "RESERVED NATIONAL USE"),

                new IFE_LLLCHAR (999, "RESERVED NATIONAL USE"  ),

                new IFE_LLLCHAR (999, "RESERVED NATIONAL USE"),

                new IFE_LLLCHAR (999, "RESERVED NATIONAL USE"),

                new IFE_LLLCHAR (999, "RESERVED NATIONAL USE"),

                new IFE_LLLCHAR (999, "RESERVED PRIVATE USE"),

                new IFE_LLLCHAR (999, "RESERVED PRIVATE USE"),

                new IFE_LLLCHAR (999, "RESERVED PRIVATE USE"),

                new IFE_LLLCHAR (999, "RESERVED PRIVATE USE"),

                new IFE_LLLCHAR (999, "RESERVED PRIVATE USE"),

                new IFE_LLLCHAR (999, "RESERVED PRIVATE USE"),

                new IFE_LLLCHAR (999, "RESERVED PRIVATE USE"),

                new IFE_LLLCHAR (999, "RESERVED PRIVATE USE"),

                new IFE_BINARY  (  8, "MAC 2")

        };

        public IPMPackager() {

            super();

            setFieldPackager(fld);

        }

    }

