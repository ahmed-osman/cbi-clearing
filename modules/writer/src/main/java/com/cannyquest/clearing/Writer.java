package com.cannyquest.clearing;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOUtil;
import org.jpos.iso.packager.GenericPackager;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.channels.OverlappingFileLockException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;


/**
 * the purpose of this class is to write the data after conversion to a file for further processing
 */
public class Writer {

    private String filePath = "out/outfile.dat";
    private RandomAccessFile raf = null;

    /**
     * instantiates the class with a designated path for the output file
     * @param file path to the desired output file location
     */



    public Writer(String file) {
        try {
            raf = new RandomAccessFile(file, "rw");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * instantiates the class with a default path for the output file
     * the default path is "out/outfile.dat"
     */
    public Writer() {
        try {
            raf = new RandomAccessFile(filePath, "rw");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }




    public void write(List<ISOMsg> ISOMsgs, String defaultPath, String pathtoPackager) throws ISOException, IOException {

        GenericPackager genericPackager = new GenericPackager(pathtoPackager);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ISOMsg wrt = new ISOMsg();
        int data = 0;
        int blockIndex = 1;
        int blockSize = 1014;
        int seed = 0;
        int remainder = 0;
        byte[] packlen = new byte[4];

        for (int i = 0; i < ISOMsgs.size(); i++) {
            wrt = ISOMsgs.get(i);
            System.out.println();
            wrt.dump(System.out, "msg-#"+i);
            wrt.setPackager(genericPackager);
            byte[] pack = wrt.pack();
            data = pack.length;
            packlen = new byte[] {
                    (byte)((data >> 24) & 0xff),
                    (byte)((data >> 16) & 0xff),
                    (byte)((data >> 8) & 0xff),
                    (byte)((data >> 0) & 0xff),
            };

            System.out.print("\nMessage Length:\t" );
            for (int i1 = 0; i1 < 4; i1++) {
                System.out.printf("%02X", packlen[i1]);
            }

            System.out.print("\nMessage:\t\t");
            for (int i1 = 0; i1 < pack.length; i1++) {
                System.out.printf("%02X", pack[i1]);
            }

            System.out.println("you are in block #:\t"+blockIndex);
            System.out.println("\nsize before writing len:\t"+outputStream.size());

            if((outputStream.size()+4)>=(blockIndex*blockSize)){
                seed = ((blockIndex*blockSize)-outputStream.size()-2);
                System.out.println("OH OH. dirty bytes.\tseed = "+seed+"\tmark = "+ (4-seed));
                outputStream.write(packlen,0,seed);
                outputStream.write(new byte[] {0x00, 0x00});
                outputStream.write(pack, seed, 4-seed);
                blockIndex++;
                System.out.println("you are leaving block #:\t" + (blockIndex-1) + "\tyou are now in block #:\t"+blockIndex);

            } else
                outputStream.write(packlen);

            System.out.println("size after writing len and before writing msg:\t"+outputStream.size()+"\twhich length is:\t"+data);
            if((outputStream.size()+data)>=(blockIndex*blockSize)){
                seed = (blockIndex*blockSize)-outputStream.size()-2;
                System.out.println("OH OH. dirty bytes.\tseed = "+seed+"\tmark = "+ (data-seed));
                outputStream.write(pack,0,seed);
                outputStream.write(new byte[] {0x00, 0x00});
                outputStream.write(pack, seed, data-(seed));
                blockIndex++;
                System.out.println("you are leaving block #:\t" + (blockIndex-1) + "\tyou are now in block #:\t"+blockIndex);

            } else
                outputStream.write(pack);


            System.out.println("size after writing ms:\t"+outputStream.size());
            System.out.println("delta:"+(outputStream.size()%1014));


        }

        byte[] delta = new byte[outputStream.size()%1014];
        outputStream.write(delta);

        byte[] bFile = outputStream.toByteArray();
        System.out.println("\nFull file:");
        for (int i = 0; i < bFile.length; i++) {
            System.out.print("byte order: " + i );
            System.out.printf("\tByte value: %02X\n",bFile[i]);
        }
        try {


            Path path = Paths.get(defaultPath);
            Files.write(path, bFile);

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    /**
     * this method locks the access to the output file then try to write the data in it.
     * @param data byte array of raw data
     */
    public void write (byte[] data){
        FileChannel channel = raf.getChannel();
        FileLock lock = null;
        try {
            lock = channel.tryLock();
        } catch (final OverlappingFileLockException e) {
            System.out.println("file is in use by another process ... ");
            try {
                raf.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            try {
                channel.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        ByteBuffer buffer = ByteBuffer.allocate(data.length);
        buffer.put(data);
        buffer.flip();
        try {
            channel.write(buffer);
        } catch (IOException e) {
            System.out.println("couldn't write to the file ...");
            e.printStackTrace();
        }
        try {
            raf.close();
        } catch (IOException e) {
            System.out.println("Couldn't close the file ...");
            e.printStackTrace();
        }
        try {
            channel.close();
        } catch (IOException e) {

            System.out.println("couldn't close the file ...");
            e.printStackTrace();
        }
    }


}
