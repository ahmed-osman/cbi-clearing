package com.cannyquest.clearing;

import org.jpos.iso.ISOMsg;


/**
 * the purpose of this class is to encapsulate the four modules taking part in clearing:
 * 1. Loader
 * 2. Reader
 * 3. Transformer
 * 4. Writer
 * an external application can deal with the Harness by passing on a path to clearing file and expect a path to
 * a transformed file in return
 */

public class Harness {
    private String clearingFilePath;

    public Harness(String clearingFilePath) {
        this.clearingFilePath = clearingFilePath;
    }




    public void process (Format srcFormat, Format dstFormat){


        Loader loader = new Loader(clearingFilePath);
        Reader reader = new Reader(loader.getBytes());
        Transformer transformer = new Transformer();
        ISOMsg srcIsoMsg = reader.getIsoMsgs().get(0);
        srcIsoMsg.dump(System.out, "src");
        ISOMsg dstIsoMsg = new ISOMsg();
        IPMPackager ipm = new IPMPackager();
        SVBOPackager sv = new SVBOPackager();

//        if ((srcFormat.equals(Format.SVBO)) && (dstFormat.equals(Format.MCIPM))){
//            dstIsoMsg.setPackager(ipm);
//            srcIsoMsg.setPackager(sv);
//        } else if((srcFormat.equals(Format.MCIPM)) && (dstFormat.equals(Format.SVBO))){
//            dstIsoMsg.setPackager(sv);
//            srcIsoMsg.setPackager(ipm);
//        }
//        srcIsoMsg.dump(System.out, "src");
//        dstIsoMsg.dump(System.out, "dst");

    }





}
