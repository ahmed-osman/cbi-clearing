package com.cannyquest.clearing;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

/**
 * the purpose of this class is to manage the raw data read from the file and load it in a buffer for further processing.
 * it performs some basic validation on the file such as making sure that the file consists of 1,014 bytes blocks of data.
 */

public class Loader

{



    /**
     * the buffer that holds the file raw data
     */
    private MappedByteBuffer buffer;

    /**
     * constructs an instance of the class by loading a specific file which path is passed as a parameter.
     * @param file path to the file required for loading
     */

    public Loader(String file ) {

        try {

            /**
             * This part of the code meant to read the incoming file. it does the same function as the library
             * CBI-Clearing.Loader
             */
            File src = new File(file);
            FileChannel fileChannel = new RandomAccessFile(src,"r").getChannel();

            /**
             * the data from the file is stored in the variable MapedByteBuffer buffer
             */


            buffer = fileChannel.map(FileChannel.MapMode.READ_ONLY, 0 , fileChannel.size());
            validateFile(fileChannel.size());








        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("File not found");
        }


    }

    /**
     * constructs an instance of the class by loading default file from location "test/source.dat"
     */

    public Loader() {

        try {
            File src = new File("test/source.dat");
            FileChannel fileChannel = new RandomAccessFile(src,"r").getChannel();
            buffer = fileChannel.map(FileChannel.MapMode.READ_ONLY, 0 , fileChannel.size());
            validateFile(fileChannel.size());




        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("File not found");
        }
    }


    /**
     *
     * @return returns the raw contents of the file in the sort of byte buffer.
     *  what happens if the file is too big? which way would be more efficient???
     *
     */


    public ByteBuffer getBytes() {
        return buffer.asReadOnlyBuffer();
    }


    /**
     * method to run basic validations on the file. it performs the following:
     * 1. making sure file is longer than 1014 byte which is the least size file possible
     * 2. making sure the file size is multiples of 1014 bytes
     * @param size
     * @throws IOException
     */

    private void validateFile(long size) throws IOException {

        if (size<1014) throw new IOException("Invalid File, length is less than 1014 byte");

        if ((size%1014) != 0) throw new IOException( "Invalid File, Length is not multiple of 1014 bytes");


    }
}