package com.cannyquest.clearing;


import org.jpos.iso.ISOException;
import org.jpos.iso.ISOField;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOUtil;

import java.util.List;

/**
 * the purpose of this class is to undergo the necessary transformation between the source and destination formats
 */

public class Transformer {



    public void transformSVBOtoTSYSIPM(List<ISOMsg> svboISOMsgs, List<ISOMsg> ipmISOMsgs) throws ISOException {

        ISOMsg tmp = new ISOMsg();

        for (int i = 0; i < svboISOMsgs.size(); i++) {

            /**
             * get the first message of the src ISOMsgs and store it in the tmp
             */
            tmp = svboISOMsgs.get(i);

            /**
             * check if the message contains any of the fields which require transformation. fields require
             * transformation are all mandatory fields hence this is an extra check the messages are valid.
             */
            if (tmp.hasField(31)) {
                /**
                 * transform DE31 from SVBO to IPM
                 */
                String strDE31 = tmp.getString(31);
                strDE31 = strDE31.substring(0, 22);
                char calcLUHN = ISOUtil.calcLUHN(strDE31);
                tmp.set(31, strDE31 + calcLUHN);

            }

            if (tmp.hasField(43)) {
                /**
                 * transform DE43 from SVBO to IPM
                 */
                String strDE31 = tmp.getString(43);
                strDE31 = strDE31.replace("/", "\\");
                tmp.set(43, strDE31);


            }

            if (tmp.hasField(48)) {
                /**
                 * transform DE48. DE48 s mandatory in all the messages and it consists of group of PDS sub-fields
                 * grouped together in TLV format.
                 */
                ISOMsg ipmDE48 = new ISOMsg(48);
               if (tmp.hasField("48.23")) {
                    ISOField PDS0023 = new ISOField(23);

                    /**
                     *   SVBO     ||     IPM
                     *   =====================
                     * 	“ATM”	  ||   “ATM”
                     * 	“POS”	  ||   “NA ”
                     * 	“EPOS”	  ||   “CT6”
                     * 	“VOI”	  ||   “NA ”
                     * 	“MPOS”	  ||   “CT9”
                     */

                    String value = tmp.getString("48.23");

                    switch (value){
                        case "ATM":
                            PDS0023.setValue("ATM");
                            break;
                        case "POS":
                            PDS0023.setValue("NA ");
                            break;
                        case "EPOS":
                            PDS0023.setValue("CT6");
                            break;
                        case "VOI":
                            System.out.println("VOI");
                            PDS0023.setValue("NA ");
                            break;
                        case "MPOS":
                            PDS0023.setValue("CT9");
                            break;
                        default:
                            System.out.println("no match");
                    }

                    ipmDE48.set(PDS0023);

                }

                if (tmp.hasField("48.25")) {


                    /**
                     * no transformation required for this field
                     */
                    ISOField PDS0025 = new ISOField(25);

                    PDS0025.setValue(tmp.getValue("48.25"));
                    ipmDE48.set(PDS0025);
                }

                if (tmp.hasField("48.105")) {
                    /**
                     * no transformation required for this field
                     */
                    ISOField PDS0105 = new ISOField(105);
                    PDS0105.setValue(tmp.getValue("48.105"));
                    ipmDE48.set(PDS0105);

                }

                if (tmp.hasField("48.122")) {
                    /**
                     * no transformation required for this field
                     */
                    ISOField PDS0122 = new ISOField(122);
                    PDS0122.setValue(tmp.getValue("48.122"));
                    ipmDE48.set(PDS0122);

                }

                if (tmp.hasField("48.165")) {

                    /**
                     * no transformation required for this field
                     */

                    ISOField PDS0165 = new ISOField(165);
                    PDS0165.setValue(tmp.getValue("48.165"));
                    ipmDE48.set(PDS0165);

                }

                if (tmp.hasField("48.177")) {

                    /**
                     * this field doesn't exist in SVBO however its mandatory in IPM
                     * its value is constant always equals "N "
                     */

                    ISOField PDS0177 = new ISOField(177);
                    PDS0177.setValue("N ");
                    ipmDE48.set(PDS0177);

                } else if(!tmp.hasField("48.177") && !((tmp.getString(24).equals("697")) || ((tmp.getString(24).equals("695"))))){

                    ISOField PDS0177 = new ISOField(177);
                    PDS0177.setValue("N ");
                    ipmDE48.set(PDS0177);

                }

                if (tmp.hasField("48.191")) {


                    /**
                     * this field doesn't exist in SVBO however its mandatory in IPM
                     * its value is constant always equals "2"
                     */
                    ISOField PDS0191 = new ISOField(191);
                    PDS0191.setValue("2");
                    ipmDE48.set(PDS0191);

                } else if(!tmp.hasField("48.191") && !((tmp.getString(24).equals("697")) || ((tmp.getString(24).equals("695"))))){

                    ISOField PDS0191 = new ISOField(191);
                    PDS0191.setValue("2");
                    ipmDE48.set(PDS0191);
                }

                if (tmp.hasField("48.262")) {


                    /**
                     * this field doesn't exist in SVBO however its mandatory in IPM
                     * its value is constant always equals "2"
                     */
                    ISOField PDS0262 = new ISOField(262);
                    PDS0262.setValue(tmp.getValue("48.262"));
                    ipmDE48.set(PDS0262);

                }

                if (tmp.hasField("48.301")) {

                    /**
                     * no transformation required for this field
                     */

                    ISOField PDS0301 = new ISOField(301);
                    PDS0301.setValue(tmp.getValue("48.301"));
                    ipmDE48.set(PDS0301);
                }

                if (tmp.hasField("48.306")) {

                    /**
                     * no transformation required for this field
                     */
                    ISOField PDS0306 = new ISOField(306);
                    PDS0306.setValue(tmp.getValue("48.306"));
                    ipmDE48.set(PDS0306);
                }

                if (tmp.hasField("48.1002")) {

                    /**
                     * this field is mandatory in SVBO but N/A in IPM. to be removed.
                     */

                    ipmDE48.unset("48.1002");
                }

                tmp.set(ipmDE48);


            }
            ipmISOMsgs.add(tmp);
        }

    }


    public void  transformTSYSIPMtoSVBO(List<ISOMsg> ipmISOMsgs, List<ISOMsg> svboISOMsgs) throws ISOException{

        /**
         * This part of the code transforms the message from IPM format to SVBO format. This role is meant to be played
         * by CB-Clearing.Transformer library
         */

        ISOMsg tmp = new ISOMsg();


        for (int i = 0; i < ipmISOMsgs.size(); i++) {

            /**
             * get the first message of the src ISOMsgs and store it in the tmp
             */
            tmp = ipmISOMsgs.get(i);

            /**
             * check if the message contains any of the fields which require transformation. fields require
             * transformation are all mandatory fields hence this is an extra check the messages are valid.
             */
            if (tmp.hasField(31)) {
                /**
                 * transform DE31 from IPM to SVBO
                 */
                String strDE31 = tmp.getString(31);
                strDE31 = strDE31.substring(1, strDE31.length()-1);
                char pad = '0';
                char prefix = '9';
                tmp.set(31, prefix+strDE31 + pad);

            }

            if (tmp.hasField(43)) {
                /**
                 * transform DE43 from IPM to SVBO
                 */
                String strDE43 = tmp.getString(43);
                strDE43 = strDE43.replace("\\", "/");
                tmp.set(43, strDE43);


            }

            if (tmp.hasField(48)) {
                /**
                 * transform DE48. DE48 s mandatory in all the messages and it consists of group of PDS sub-fields
                 * grouped together in TLV format.
                 */
                ISOMsg svboDE48 = new ISOMsg(48);

                if (tmp.hasField("48.0002")) {


                    /**
                     * this field doesn't exist in SVBO however its mandatory in IPM
                     * to be removed
                     */
                    svboDE48.unset("48.0002");



                }

                if (tmp.hasField("48.0003")) {


                    /**
                     * this field doesn't exist in SVBO however its mandatory in IPM
                     * to be removed
                     */
                    svboDE48.unset("48.0003");



                }



                if (tmp.hasField("48.23")) {
                    ISOField PDS0023 = new ISOField(23);

                    /**
                     *   SVBO     ||     IPM
                     *   =====================
                     * 	“ATM”	  ||   “ATM”
                     * 	“POS”	  ||   “NA ”
                     * 	“EPOS”	  ||   “CT6”
                     * 	“VOI”	  ||   “NA ”
                     * 	“MPOS”	  ||   “CT9”
                     */

                    String value = tmp.getString("48.23");

                    switch (value){
                        case "ATM":
                            System.out.println("ATM");
                            PDS0023.setValue("ATM");
                            break;
                        case "NA ":
                            System.out.println("POS");
                            PDS0023.setValue("POS");

                            break;
                        case "NA":
                            System.out.println("POS");
                            PDS0023.setValue("POS");

                            break;
                        case "CT6":
                            System.out.println("EPOS");
                            PDS0023.setValue("EPOS");

                            break;
                        case "VOI":
                            System.out.println("VOI");
                            PDS0023.setValue("NA ");

                            break;
                        case "CT9":
                            System.out.println("MPOS");
                            PDS0023.setValue("MPOS");

                            break;
                        default:
                            PDS0023.setValue("NA");
                            System.out.println("no match");
                    }

                    svboDE48.set(PDS0023);

                } else {
                    ISOField PDS0023 = new ISOField(23);

                    if (tmp.hasField(3) && tmp.getString(3).startsWith("00")){
                        PDS0023.setValue("POS");
                        svboDE48.set(PDS0023);

                    } else if (tmp.hasField(3) && !tmp.getString(3).startsWith("00")){
                        PDS0023.setValue("NA");
                        svboDE48.set(PDS0023);

                    } else
                        svboDE48.unset("48.23");



                }

                if (tmp.hasField("48.25")) {


                    /**
                     * not expected by CBI in outgoing file, must be removed
                     */

                    svboDE48.unset("48.25");



                }

                if (tmp.hasField("48.105")) {
                    /**
                     * no transformation required for this field
                     */
                    ISOField PDS0105 = new ISOField(105);
                    PDS0105.setValue(tmp.getValue("48.105"));
                    svboDE48.set(PDS0105);


                }

                if (tmp.hasField("48.122")) {
                    /**
                     * no transformation required for this field
                     */
                    ISOField PDS0122 = new ISOField(122);
                    PDS0122.setValue(tmp.getValue("48.122"));
                    svboDE48.set(PDS0122);

                }

                if (tmp.hasField("48.0148")) {


                    /**
                     * this field doesn't exist in SVBO however its mandatory in IPM
                     * to be removed
                     */
                    svboDE48.unset("48.0148");



                }

                if (tmp.hasField("48.0158")) {


                    /**
                     * this field doesn't exist in SVBO however its mandatory in IPM
                     * to be removed
                     */
                    svboDE48.unset("48.0158");



                }

                if (tmp.hasField("48.165")) {

                    /**
                     * no transformation required for this field
                     */

                    ISOField PDS0165 = new ISOField(165);
                    PDS0165.setValue(tmp.getValue("48.165"));
                    svboDE48.set(PDS0165);

                }

                if (tmp.hasField("48.177")) {

                    /**
                     * this field doesn't exist in SVBO however its mandatory in IPM
                     * to be removed
                     */
                    svboDE48.unset("48.177");

                }




                if (tmp.hasField("48.191")) {


                    /**
                     * this field doesn't exist in SVBO however its mandatory in IPM
                     * to be removed
                     */
                    svboDE48.unset("48.191");



                }

                if (tmp.hasField("48.262")) {

                    /**
                     * no transformation required for this field
                     */

                    ISOField PDS0262 = new ISOField(262);
                    PDS0262.setValue(tmp.getValue("48.262"));
                    svboDE48.set(PDS0262);
                }

                if (tmp.hasField("48.301")) {

                    /**
                     * no transformation required for this field
                     */

                    ISOField PDS0301 = new ISOField(301);
                    PDS0301.setValue(tmp.getValue("48.301"));
                    svboDE48.set(PDS0301);

                }

                if (tmp.hasField("48.306")) {

                    /**
                     * no transformation required for this field
                     */

                    ISOField PDS0306 = new ISOField(306);
                    PDS0306.setValue(tmp.getValue("48.306"));
                    svboDE48.set(PDS0306);

                }


                if (!tmp.hasField("48.1002")){
                    ISOField PDS1002 = new ISOField(1002);
                    if(tmp.hasField(3)){
                        String value = tmp.getString(3).substring(0,2);

                        switch (value){
                            case "20":
                                PDS1002.setValue("775");
                                break;
                            case "01":
                                PDS1002.setValue("700");
                                break;
                            case "17":
                                PDS1002.setValue("700");
                                break;
                            /**
                             * this assumes that if PCODE starts with 00 then it is a purchase trx.
                             * caveat: how to identify a pre-auth transaction.
                             */
                            case "00":
                                PDS1002.setValue("774");
                                break;
                        }

                        svboDE48.set(PDS1002);


                    }
                }



                tmp.set(svboDE48);


            }
            svboISOMsgs.add(tmp);
        }
    }


    /**
     * this method transforms the content of DE31.
     * in SVBO the last char of the field is '0' while in MCIPM the last char is the luhn check on the proceeding 22 chars
     * @param srcFormat
     * @param dstFormat
     * @param srcDE31
     * @return transformed DE31
     * @throws ISOException
     */

    public String transformDE31 (Format srcFormat, Format dstFormat, String srcDE31) throws ISOException {

        if (srcDE31.length()<31)
            throw  new ISOException("DE31 size is wrong ...");

        String dstDE31=null, tmp=srcDE31.substring(0,22);

        if (srcFormat.equals(Format.SVBO) && dstFormat.equals(Format.MCIPM)){
            return new StringBuilder(tmp).append(ISOUtil.calcLUHN("tmp")).toString();

        } else if (srcFormat.equals(Format.MCIPM) && dstFormat.equals(Format.SVBO)){

            return new StringBuilder(tmp).append('0').toString();
        }
        return null;

    }

    /**
     * this method transforms DE43
     * in SVBO the delimiter value used is '/' while in MCIPM it is '\'
     * PRIME won't support the last three fields hence the engine will supplement them
     */





    public String transformDE43(Format srcFormat, Format dstFormat, String srcDE43){


        char[] tmp = srcDE43.toCharArray();

        if (srcFormat.equals(Format.SVBO) && dstFormat.equals(Format.MCIPM)){

            /**
             * change the delimiter value
             */

            for (int i = 0; i < srcDE43.toCharArray().length; i++) {
                if(tmp[i] == '/')
                    tmp[i] = '\\';
            }

            /**
             * add extra 3 bytes at the end of sub-field 2
             */

            String str = tmp.toString();
            StringBuilder bldr = new StringBuilder(str.substring(0,68)).append("000").append(str.substring(69,str.length()));

            return bldr.toString();

        } else if (srcFormat.equals(Format.MCIPM) && dstFormat.equals(Format.SVBO)){


            /**
             * change the delimiter value
             */

            for (int i = 0; i < srcDE43.toCharArray().length; i++) {
                if(tmp[i] == '\\')
                    tmp[i] = '/';
            }

            /**
             * remove extra 3bytes in subfield 2
             */



            /**
             * add the missing fields
             */

            String val = tmp.toString().substring(0,83);
            StringBuilder bld = new StringBuilder(val);
            bld.append("8274653890").append("BGD").append("IRQ");

            return bld.toString();
        }

        return null;
    }


    /**
     * this method transforms method DE48
     * changes are in subfield 0023 (EPOS = CT6, VOI = NA , MPOS = CT9)
     * and in subfeild 1002 ( null in IPM and equals transaction code in SVBO)
     */

    public String transformDE48(Format srcFormat, Format dstFormat, String srcDE43){


        if (srcFormat.equals(Format.SVBO) && dstFormat.equals(Format.MCIPM)){

            String dstDE43 = srcDE43.replaceFirst("EPOS","CT6").replaceFirst("VOI","NA").replaceFirst("MPOS","CT9");



        } else if (srcFormat.equals(Format.MCIPM) && dstFormat.equals(Format.SVBO)){

            String dstDE43 = srcDE43.replaceFirst("CT6","EPOS").replaceFirst("NA","VOI").replaceFirst("CT9","MPOS");



        }


        return null;
    }

}
