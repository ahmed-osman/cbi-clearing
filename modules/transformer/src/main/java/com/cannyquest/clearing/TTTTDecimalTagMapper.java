package com.cannyquest.clearing;

import org.jpos.iso.packager.TagMapper;

import java.util.HashMap;
import java.util.Map;

public class TTTTDecimalTagMapper implements TagMapper {

    public TTTTDecimalTagMapper() {
    }

    private Map<String, Integer> tagToNumberMap = new HashMap<String, Integer>();
    private  Map<String, String> numberToTagMap = new HashMap<String, String>();

    @Override
    public String getTagForField(int fieldNumber, int subFieldNumber) {
        return numberToTagMap.get(fieldNumber + "." + subFieldNumber);
    }

    @Override
    public Integer getFieldNumberForTag(int fieldNumber, String tag) {
        return tagToNumberMap.get(fieldNumber + "." + tag);
    }
}