package com.cannyquest.jpos;

import org.jpos.iso.packager.TagMapper;

import java.util.HashMap;
import java.util.Map;

public class TagMapperImpl implements TagMapper {

    private static Map<String, Integer> tagToNumberMap = new HashMap<String, Integer>();
    private static Map<String, String> numberToTagMap = new HashMap<String, String>();

    static {
        tagToNumberMap.put("105", 1);
        numberToTagMap.put("48.0105", "105");

        tagToNumberMap.put("48.A9", 2);
        numberToTagMap.put("48.2", "A2");

        tagToNumberMap.put("48.A3", 3);
        numberToTagMap.put("48.3", "A3");

        tagToNumberMap.put("60.A1", 1);
        numberToTagMap.put("60.1", "A1");
    }

    public TagMapperImpl() {
    }

    public String getTagForField(int fieldNumber, int subFieldNumber) {
        return numberToTagMap.get(fieldNumber + "." + subFieldNumber);
    }

    public Integer getFieldNumberForTag(int fieldNumber, String tag) {
        return tagToNumberMap.get(fieldNumber + "." + tag);
    }

}
